$(document).ready(function(){
	var height_content = $('.entry_wrapper:first').height();
	var content_width = $('.entry_wrapper:first').width();
	var complete_width = 0;
	var slide_width = 0;
	var margin_css = 0;
	var left_active = true;
	var right_active = true;
	var i = 0;
	
	$('.contentText').children().css('text-align', 'initial');
	$('.contentText').children().css('font-family', 'Tahoma, Geneva, sans-serif');
	$('code').css('font-family', 'Tahoma, Geneva, sans-serif');
	function iframe_loaded() {
	    if ($('iframe').length > 0) {
	        $("iframe").contents().find("#tinymce h5").css('font-family', 'Tahoma, Geneva, sans-serif');
	        $("iframe").contents().find("#tinymce h5").css('font-size', '12px');
	        $("iframe").contents().find("#tinymce pre").css('font-family', 'Tahoma, Geneva, sans-serif');
	        $("iframe").contents().find("#tinymce pre").css('font-size', '12px');
	    }
	    else {
	        setTimeout(iframe_loaded, 100);
	    }
	}
	
	iframe_loaded();
	
	$('.entry_wrapper').each(function(){
		complete_width = $(this).width() + complete_width + 40;
		if($(this).height() > height_content)
			height_content = $(this).height();
	});
	
	$('.contentPic').css('margin-left', content_width - 480);
	
	$('.entry_wrapper').width(content_width - 40);
	$('.entry_wrapper').css('margin-right', 80);
	$('.contentText').width(content_width - 600);
	
	$('.slide_wrapper').width(complete_width);
	
	if(parseInt($('.slide_wrapper').css('margin-left')) < 0)
		$('.arrow.left').css('opacity', 1);
		
	margin_css = (parseInt($('.slide_wrapper').css('margin-left')) - content_width -40) * -1 + 1; 
	if(margin_css < complete_width)
		$('.arrow.right').css('opacity', 1);
		
	margin_css = (parseInt($('.slide_wrapper').css('margin-left')) - content_width -40) * -1; 
	if(margin_css < complete_width && right_active === true && location.hash != ""){
		right_active = false;
		i = parseInt(location.hash.substring(1));
		$('.arrow.right').css('opacity', 0.4);
		$('.slide_wrapper').css('margin-left', (content_width + 40) * parseInt(location.hash.substring(1)) * -1);
		setTimeout(function(){
	        right_active = true;
	        margin_css = (parseInt($('.slide_wrapper').css('margin-left')) - content_width -40) * -1 + 1; 
	        if(margin_css < complete_width)
	        	$('.arrow.right').css('opacity', 1);
	        if(parseInt($('.slide_wrapper').css('margin-left')) < 0)
	        	$('.arrow.left').css('opacity', 1);
	    }, 1000);

	}
		
		
	
	$('.arrow.left').click(function(){
		if(parseInt($('.slide_wrapper').css('margin-left')) < 0 && left_active === true){
			left_active = false;
			i = i - 1;
			x = i + 1;
			location.hash = i;
			//$('.entry_box').height($('.entry_wrapper:nth-child('+x+')').height() + 30);
			$('.arrow.left').css('opacity', 0.4);
			slide_width = parseInt($('.slide_wrapper').css('margin-left')) + content_width + 40;
			$('.slide_wrapper').css('margin-left', (content_width + 40) * -i);
			setTimeout(function(){
		        left_active = true;
		        if(parseInt($('.slide_wrapper').css('margin-left')) < 0)
		     		$('.arrow.left').css('opacity', 1);
		     	margin_css = (parseInt($('.slide_wrapper').css('margin-left')) - content_width -40) * -1 + 1; 
		        if(margin_css < complete_width)
		        	$('.arrow.right').css('opacity', 1);
		    }, 1000);
		}
	});
	
	$('.arrow.right').click(function(){
		margin_css = (parseInt($('.slide_wrapper').css('margin-left')) - content_width -40) * -1; 
		if(margin_css < complete_width && right_active === true){
			right_active = false;
			i = i + 1;
			x = i + 1;
			location.hash = i;
			//$('.entry_box').height($('.entry_wrapper:nth-child('+x+')').height() + 30);
			$('.arrow.right').css('opacity', 0.4);
			$('.slide_wrapper').css('margin-left', (content_width + 40) * i * -1);
			setTimeout(function(){
		        right_active = true;
		        margin_css = (parseInt($('.slide_wrapper').css('margin-left')) - content_width -40) * -1 + 1; 
		        if(margin_css < complete_width)
		        	$('.arrow.right').css('opacity', 1);
		        if(parseInt($('.slide_wrapper').css('margin-left')) < 0)
		        	$('.arrow.left').css('opacity', 1);
		    }, 1000);

		}
	});
	
	$('.vorschau_pic').click(function(){
		location.hash = $(this).attr('data-counter');
		
		margin_css = (parseInt($('.slide_wrapper').css('margin-left')) - content_width -40) * -1; 
		if(margin_css < complete_width && right_active === true){
			right_active = false;
			i = parseInt(location.hash.substring(1));
			x = location.hash.substring(1) + 1;
			$('.arrow.right').css('opacity', 0.4);
			$('.slide_wrapper').css('margin-left', (content_width + 40) * parseInt(location.hash.substring(1)) * -1);
			setTimeout(function(){
		        right_active = true;
		        margin_css = (parseInt($('.slide_wrapper').css('margin-left')) - content_width -40) * -1 + 1; 
		        if(margin_css < complete_width)
		        	$('.arrow.right').css('opacity', 1);
		        if(parseInt($('.slide_wrapper').css('margin-left')) < 0)
		        	$('.arrow.left').css('opacity', 1);
		    }, 1000);

		}
	});
	
	var height_width = 0;
	
	$('.show_big').click(function(){
		$('body').append('<div class="overlay"></div>');
		
		if($(this).attr('data-pic') != "")
			$('body').append('<img height="80%" class="img_popup" src="'+$(this).attr('data-pic')+'">');
		else
			$('body').append('<img height="80%" class="img_popup" src="'+$(this).attr('src')+'">');
			
		$('.img_popup').load(function(){
			pic_width = ($(document).width() - $('.img_popup').width()) / 2;
			$('.img_popup').css('left', pic_width);
		});
		
		$('.img_popup, .overlay').click(function(){
			$('.img_popup').remove();
			$('.overlay').remove();
		});
	});
	
	
	
	$( window ).resize(function() {
		$('.entry_wrapper').width($('.entry_box').width() + 40);
		
		height_content = $('.entry_wrapper:first').height();
		content_width = $('.entry_wrapper:first').width();
		complete_width = 0;
		slide_width = 0;
		margin_css = 0;
		left_active = true;
		right_active = true;
	
		$('.entry_wrapper').each(function(){
			complete_width = $(this).width() + complete_width + 40;
		});
		
		
		$('.contentText').width(content_width - 600);
		$('.entry_wrapper').width(content_width - 40);
		$('.entry_wrapper').css('margin-right', 80);
		$('.slide_wrapper').width(complete_width);
		
		$('.contentPic').css('margin-left', content_width - 480);
		$('.slide_wrapper').css('margin-left', (content_width + 40) * i * -1);
		
		height_width = ($(document).width() - $('.img_popup').width()) / 2;
		$('.img_popup').css('left', height_width);
	});
});

