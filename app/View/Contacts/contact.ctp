<?php
	echo $this->Form->create("Contact");
	echo $this->Form->input('name', array("label" => "Name", 'max' => 255));
	echo $this->Form->input('email', array("label" => "Email-Adresse", 'max' => 255));
	echo $this->Form->input('telephone', array("label" => "Telefon (*nicht erforderlich)", 'max' => 255));
	echo $this->Form->input('message', array("label" => "Nachricht", 'type' => "textarea"));
	echo $this->Form->end('Abschicken', array('value' => 'Abschicken'));