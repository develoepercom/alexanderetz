<div class="posts form"> 
<?php echo $this->Form->create($cat, array('type' => 'file'));?> 
    <fieldset> 
    	<?php 
    	if($this->params['action'] == "edit")
			$legend_title = __("Eintrag bearbeiten");    		
		else if($this->params['action'] == "create")
			$legend_title = __("Eintrag erstellen");
    	?>
        	<legend><?php echo $legend_title; ?></legend> 
    <?php 
     
        echo $this->Form->input('year', array("label" => "Jahreszahl", 'type' => 'number')); 
    	echo $this->Form->input($cat.'.upload_1', array('type' => 'file', 'label' => 'Bild-1 (Nur .jpg /.jpeg /.png)'));
		echo $this->Form->input($cat.'.upload_2', array('type' => 'file', 'label' => 'Bild-2 (Nur .jpg /.jpeg /.png)'));
		echo $this->Form->input($cat.'.upload_3', array('type' => 'file', 'label' => 'Bild-3 (Nur .jpg /.jpeg /.png)'));
		echo $this->Form->input($cat.'.upload_4', array('type' => 'file', 'label' => 'Bild-4 (Nur .jpg /.jpeg /.png)'));
		echo $this->Form->input($cat.'.upload_5', array('type' => 'file', 'label' => 'Bild-5 (Nur .jpg /.jpeg /.png)'));
        echo $this->Form->input('title', array("label" => "Titel", 'max' => 255)); 
        echo $this->Tinymce->input($cat.'.text', array('label' => 'Inhalt'), array('language'=>'de'), 'bbcode'); 
		if($this->params['action'] == "edit")
			echo $this->Form->input('is_deleted', array("label" => "Eintrag löschen", "type" => "checkbox")); 
    ?> 
    </fieldset> 
<?php echo $this->Form->end('Abschicken', array('value' => 'Abschicken'));?> 
</div>