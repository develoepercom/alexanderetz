<?php
$i = 0;
$x = 0;
$y = 0;
$c = 0;
echo '<div class="entry_box">';
	if(count($entrys) > 1)
		echo '<img src="/images/arrow_left.png" width="50px" class="arrow left">';
	if($_authUserID > 0 && $this->params['controller'] != "archives")
		echo '<a href="/'.$cat.'s/create">Erstellen</a><br/>';
	
	echo '<div class="slide_wrapper">';
		echo '<div class="entry_wrapper">';
		if($_authUserID > 0 && $this->params['controller'] != "archives")
			echo '<a href="/'.$cat.'s/title/">Titel/Preisanfrage</a><br/>';
		if(isset($title_price['Headline']) && $title_price['Headline'] != "")
			echo '<h1>'.$title_price['Headline']['title'].'</h1>';
			foreach($entrys as $key => $entry){
				
				$y++;
				
				$i++;
				echo '<div class="pic_vorschau">';
					if($entry[$cat]['pic_link_1'] != "")
						echo '<img class="vorschau_pic" data-counter="'.$i.'" height="100px" src="'.$entry[$cat]['pic_link_1'].'"/>';
					else if($entry[$cat]['pic_link_2'] != "")
						echo '<img class="vorschau_pic" data-counter="'.$i.'" height="100px" src="'.$entry[$cat]['pic_link_2'].'"/>';
					else if($entry[$cat]['pic_link_3'] != "")
						echo '<img class="vorschau_pic" data-counter="'.$i.'" height="100px" src="'.$entry[$cat]['pic_link_3'].'"/>';
					else if($entry[$cat]['pic_link_4'] != "")
						echo '<img class="vorschau_pic" data-counter="'.$i.'" height="100px" src="'.$entry[$cat]['pic_link_4'].'"/>';
					else if($entry[$cat]['pic_link_5'] != "")
						echo '<img class="vorschau_pic" data-counter="'.$i.'" height="100px" src="'.$entry[$cat]['pic_link_5'].'"/>';
						
					if($entry[$cat]['year'] != "")
						echo '<br/><span>'.$entry[$cat]['year'].'</span>';
					else
						echo '<br/><span>keine Angabe</span>';
						
				echo '</div>';
			} ?>
			<br style="clear:both;"/><h3 style="margin-left: 25px;"><?php echo $title_price['Headline']['price_note']; ?></h3>
			<?php
		echo '</div>';
			foreach($entrys as $key => $entry){
				$c = 0;
				echo '<div class="entry_wrapper">';
					if($_authUserID > 0)
						echo '<a href="/'.$cat.'s/edit/'.$entry[$cat]['id'].'">Bearbeiten</a>';
					echo '<h1>'.$entry[$cat]['title'].'</h1>';
					echo '<div class="contentText">';
						echo $entry[$cat]['text'];
					echo '</div>';
					echo '<div class="contentPic jcarousel-wrapper">';
						echo '<div data-jcarousel="true" data-wrap="circular" class="jcarousel"><ul>';
							if($entry[$cat]['pic_link_1'] != ""){
								$c++; echo '<li><img class="show_big" data-pic="'.$entry[$cat]['pic_link_1'].'" width="400" src="'.$entry[$cat]['pic_link_1'].'"/></li>'; }
							if($entry[$cat]['pic_link_2'] != ""){
								$c++; echo '<li><img class="show_big" data-pic="'.$entry[$cat]['pic_link_2'].'" width="400" src="'.$entry[$cat]['pic_link_2'].'"/></li>'; }
							if($entry[$cat]['pic_link_3'] != ""){
								$c++; echo '<li><img class="show_big" data-pic="'.$entry[$cat]['pic_link_3'].'" width="400" src="'.$entry[$cat]['pic_link_3'].'"/></li>'; }
							if($entry[$cat]['pic_link_4'] != ""){
								$c++; echo '<li><img class="show_big" data-pic="'.$entry[$cat]['pic_link_4'].'" width="400" src="'.$entry[$cat]['pic_link_4'].'"/></li>'; }
							if($entry[$cat]['pic_link_5'] != ""){
								$c++; echo '<li><img class="show_big" data-pic="'.$entry[$cat]['pic_link_5'].'" width="400" src="'.$entry[$cat]['pic_link_5'].'"/></li>'; }
							
						echo '</ul></div>';
						if($c > 1){
							echo '<a data-jcarousel-control="true" data-target="-=1" href="#" class="jcarousel-control-prev">&lsaquo;</a>
	                			<a data-jcarousel-control="true" data-target="+=1" href="#" class="jcarousel-control-next">&rsaquo;</a>';	
						}
						
					echo '</div>';
					
				echo '</div>';
				
			}
	echo '</div>';
	if(count($entrys) > 1)
		echo '<img src="/images/arrow_right.png" width="50px" class="arrow right">';
echo '</div>';



?>

<script>
	
(function($) {
    $(function() {
        $('[data-jcarousel]').each(function() {
            var el = $(this);
            el.jcarousel(el.data());
        });

        $('[data-jcarousel-control]').each(function() {
            var el = $(this);
            el.jcarouselControl(el.data());
        });
    });
})(jQuery);

</script>
