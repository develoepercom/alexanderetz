<?php
$i = 0;
$x = 0;
$y = 0;
$c = 0;
echo '<div class="entry_box">';
	if(count($entrys) > 1)
		echo '<img src="/images/arrow_left.png" width="50px" class="arrow left">';
	if($_authUserID > 0 && $this->params['controller'] != "archives")
		echo '<a href="/'.$cat.'s/create">Erstellen</a><br/>';
	echo '<div class="slide_wrapper">';
			foreach($entrys as $key => $entry){
				$c = 0;
				echo '<div class="entry_wrapper">';
					if($_authUserID > 0)
						echo '<a href="/'.$cat.'s/edit/'.$entry[$cat]['id'].'">Bearbeiten</a>';
					echo '<h1>'.$entry[$cat]['title'].'</h1>';
					echo '<div class="contentText">';
						echo $entry[$cat]['text'];
					echo '</div>';
					echo '<div class="contentPic">';
						echo '<div class="jcarousel"><ul>';
							$clear = "";
							if($entry[$cat]['pic_link_1'] != ""){
								$c++;
								echo '<li style="'.$clear.'"><img class="show_big" data-pic="'.$entry[$cat]['pic_link_1'].'" height="200" src="'.$entry[$cat]['pic_link_1'].'"/></li>'; }
							if($entry[$cat]['pic_link_2'] != ""){
								$c++;
								if($c == 3) {$clear = "clear: both";}else{$clear = "";}
								echo '<li style="'.$clear.'"><img class="show_big" data-pic="'.$entry[$cat]['pic_link_2'].'" height="200" src="'.$entry[$cat]['pic_link_2'].'"/></li>'; }
							if($entry[$cat]['pic_link_3'] != ""){
								$c++;
								if($c == 3) {$clear = "clear: both";}else{$clear = "";}
								echo '<li style="'.$clear.'"><img class="show_big" data-pic="'.$entry[$cat]['pic_link_3'].'" height="200" src="'.$entry[$cat]['pic_link_3'].'"/></li>'; }
							if($entry[$cat]['pic_link_4'] != ""){
								$c++;
								if($c == 3) {$clear = "clear: both";}else{$clear = "";}
								echo '<li style="'.$clear.'"><img class="show_big" data-pic="'.$entry[$cat]['pic_link_4'].'" height="200" src="'.$entry[$cat]['pic_link_4'].'"/></li>'; }

						echo '</ul></div>';
					echo '</div>';
					
				echo '</div>';
				
			}
	echo '</div>';
	if(count($entrys) > 1)
		echo '<img src="/images/arrow_right.png" width="50px" class="arrow right">';
echo '</div>';



?>

<script>
	
(function($) {
    $(function() {
        $('[data-jcarousel]').each(function() {
            var el = $(this);
            el.jcarousel(el.data());
        });

        $('[data-jcarousel-control]').each(function() {
            var el = $(this);
            el.jcarouselControl(el.data());
        });
    });
})(jQuery);

</script>
