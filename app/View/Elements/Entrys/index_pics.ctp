<?php
$i = 0;
$x = 0;
$y = 0;
echo '<div class="entry_box">';
	if(count($entrys) > 1)
		echo '<img src="/images/arrow_left.png" width="50px" class="arrow left">';
	if($_authUserID > 0 && $this->params['controller'] != "archives")
		echo '<a href="/'.$cat.'s/create">Erstellen</a><br/>';
	echo '<div class="slide_wrapper">';
		echo '<div class="entry_wrapper">';
			if($_authUserID > 0 && $this->params['controller'] != "archives")
				echo '<a href="/'.$cat.'s/title/">Titel/Preisanfrage</a><br/>';
			else if($_authUserID > 0 && $this->params['controller'] == "archives")
				echo '<a href="/'.$cat.'s/title_archive/">Titel/Preisanfrage</a><br/>';

			if(isset($title_price['Headline']) && $title_price['Headline'] != "")
				echo '<h1>'.$title_price['Headline']['title'].'</h1>';

			foreach($entrys as $key => $entry){
				$y++;
				if(($this->params['controller'] == "archives" && $y > 16) || $this->params['controller'] != "archives"){
					$i++;
					echo '<div class="pic_vorschau">';
						if($entry[$cat]['pic_link'] != "")
							echo '<img class="vorschau_pic" data-counter="'.$i.'" data-pic="'.$entry[$cat]['pic_big_link'].'" height="100px" src="'.$entry[$cat]['pic_link'].'"/>';
						if($entry[$cat]['title'] != "")
							echo '<br/><span>'.$entry[$cat]['title'].'</span>';
					echo '</div>';
				}
			} ?>
			<br style="clear:both;"/><h3 style="margin-left: 25px;"><?php echo $title_price['Headline']['price_note']; ?></h3>
		</div> <?php
			foreach($entrys as $key => $entry){
				$x++;
				if(($this->params['controller'] == "archives" && $x > 16) || $this->params['controller'] != "archives"){ ?>
					<div class="entry_wrapper">
						<?php if($_authUserID > 0)
							echo '<a href="/'.$cat.'s/edit/'.$entry[$cat]['id'].'">Bearbeiten</a>';
						echo '<h1>'.$entry[$cat]['title'].'</h1>'; ?>
						<div class="contentText"> <?php
							echo $entry[$cat]['text'];
							echo '<span class="price_note">'.$entry[$cat]['price_note'].'</span>'; ?>
						</div>
					
						<div class="contentPic"><?php
							if($entry[$cat]['pic_link'] != "")
								echo '<img class="show_big" data-pic="'.$entry[$cat]['pic_big_link'].'" width="400px" src="'.$entry[$cat]['pic_link'].'"/>';
							else{
								echo '<img width="400px" src="/images/pic_platzhalter.jpg"/>';
							}
							if($entry[$cat]['pic_note_link'] != "")
								echo '<a href="'.$entry[$cat]['pic_note_link'].'">';
							echo '<span>'.$entry[$cat]['pic_note'].'</span>';
							if($entry[$cat]['pic_note_link'] != "")
								echo '</a>'; ?>
						</div>
					</div> <?php
				}
			} ?>
	</div> <?php
	if(count($entrys) > 1)
		echo '<img src="/images/arrow_right.png" width="50px" class="arrow right">'; ?>
</div>
