<?php
echo '<div class="entry_box">';
	if(count($entrys) > 1)
		echo '<img src="/images/arrow_left.png" width="50px" class="arrow left">';
	if($_authUserID > 0)
		echo '<a href="/'.$cat.'s/create">Erstellen</a><br/>';
	echo '<div class="slide_wrapper">';
		foreach($entrys as $key => $entry){
			echo '<div class="entry_wrapper">';
				if($_authUserID > 0)
					echo '<a href="/'.$cat.'s/edit/'.$entry[$cat]['id'].'">Bearbeiten</a>';
				echo '<h1>'.$entry[$cat]['title'].'</h1>';
				echo '<div class="contentText">';
					echo $entry[$cat]['text'];
				echo '</div>';
				echo '<div class="contentPic">';
				if($entry[$cat]['pic_link'] != "")
					echo '<img class="show_big" data-pic="'.$entry[$cat]['pic_big_link'].'" width="400px" src="'.$entry[$cat]['pic_link'].'"/>';
				else{
					echo '<img width="400px" src="/images/pic_platzhalter.jpg"/>';
				}
				if($entry[$cat]['pic_note_link'] != "")
					echo '<a href="'.$entry[$cat]['pic_note_link'].'">';
				echo '<span>'.$entry[$cat]['pic_note'].'</span>';
				if($entry[$cat]['pic_note_link'] != "")
					echo '</a>';
				echo '</div>';
			echo '</div>';
		}
	echo '</div>';
	if(count($entrys) > 1)
		echo '<img src="/images/arrow_right.png" width="50px" class="arrow right">';
echo '</div>';
