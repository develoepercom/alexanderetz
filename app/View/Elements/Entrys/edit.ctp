<div class="posts form"> 
<?php echo $this->Form->create($cat, array('type' => 'file'));?> 
    <fieldset> 
    	<?php 
    	if($this->params['action'] == "edit")
			$legend_title = __("Eintrag bearbeiten");    		
		else if($this->params['action'] == "create")
			$legend_title = __("Eintrag erstellen");
    	?>
        	<legend><?php echo $legend_title; ?></legend> 
    <?php 
    	echo $this->Form->input($cat.'.upload', array('type' => 'file', 'label' => 'Bild-Vorschau (Nur .jpg /.jpeg /.png)'));
	echo $this->Form->input($cat.'.big_upload', array('type' => 'file', 'label' => 'Bild-Groß (Nur .jpg /.jpeg/.png)'));
	echo $this->Tinymce->input($cat.'.pic_note', array("label" => "Bild Anmerkung"), array('language'=>'de'), 'bbcode');
	echo $this->Form->input('pic_note_link', array("label" => "Bild Anmerkung URL"));
     echo $this->Form->input('title', array("label" => "Titel", 'max' => 255));
     echo $this->Tinymce->input($cat.'.text', array('label' => 'Inhalt'), array('language'=>'de'), 'bbcode');
	if($this->params['controller'] == "Pics")
		echo $this->Form->input('price_note', array("label" => "Preis-Formular", 'max' => 255));
	if($this->params['action'] == "edit")
		echo $this->Form->input('is_deleted', array("label" => "Eintrag löschen", "type" => "checkbox"));
    ?> 
    </fieldset> 
<?php echo $this->Form->end('Abschicken', array('value' => 'Abschicken'));?> 
</div>