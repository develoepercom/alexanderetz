<?php
	$nav = array();
	$submenu = array();
	
	$nav['atelier'] = array("Name" => __("Atelier"), "Link" => $this->Html->url(array("controller" => "ateliers", "action" => "index")), "active" => ($this->params['controller'] == "ateliers" || $this->params['controller'] == "roundwalks"));
		$submenu['atelier']['rundgang'] = array("Name" => __($roundwalk_title), "Link" => $this->Html->url(array("controller" => "roundwalks", "action" => "index")), "active" => ($this->params['controller'] == "roundwalks" && $this->params['action'] == "index"));
	$nav['vita'] = array("Name" => __("Vita"), "Link" => $this->Html->url(array("controller" => "vitas", "action" => "index")), "active" => ($this->params['controller'] == "vitas" || $this->params['controller'] == "presentations"));
		$submenu['vita']['presentation'] = array("Name" => __("Präsentationen"), "Link" => $this->Html->url(array("controller" => "presentations", "action" => "index")), "active" => ($this->params['controller'] == "presentations" && $this->params['action'] == "index"));
	$nav['new_pics'] = array("Name" => __("neue Bilder"), "Link" => $this->Html->url(array("controller" => "pics", "action" => "index")), "active" => ($this->params['controller'] == "pics" && $this->params['action'] == "index"));
	$nav['archiv'] = array("Name" => __("Archiv"), "Link" => $this->Html->url(array("controller" => "archives", "action" => "index")), "active" => ($this->params['controller'] == "archives" && $this->params['action'] == "index"));
	$nav['sammlungen'] = array("Name" => __("in Sammlungen"), "Link" => $this->Html->url(array("controller" => "collections", "action" => "index")), "active" => ($this->params['controller'] == "collections" && $this->params['action'] == "index"));
	$nav['texte'] = array("Name" => __("Texte"), "Link" => $this->Html->url(array("controller" => "textes", "action" => "index")), "active" => ($this->params['controller'] == "textes" && $this->params['action'] == "index"));
	$nav['contact'] = array("Name" => __("Kontakt"), "Link" => $this->Html->url(array("controller" => "contacts", "action" => "contact")), "active" => ($this->params['controller'] == "contacts"));
		$submenu['contact']['impressum'] = array("Name" => __("Impressum"), "Link" => $this->Html->url(array("controller" => "contacts", "action" => "impressum")), "active" => ($this->params['controller'] == "contacts" && $this->params['action'] == "impressum"));
	if($_authUserID > 0){
		$nav['admin'] = array("Name" => __("Admin"), "Link" => $this->Html->url(array("controller" => "users", "action" => "add")), "active" => ($this->params['controller'] == "users"));
			$submenu['admin']['add'] = array("Name" => __("Benutzer anlegen"), "Link" => $this->Html->url(array("controller" => "users", "action" => "add")), "active" => ($this->params['controller'] == "users" && $this->params['action'] == "add"));
			$submenu['admin']['Logout'] = array("Name" => __("Ausloggen"), "Link" => $this->Html->url(array("controller" => "users", "action" => "logout")), "active" => ($this->params['controller'] == "users" && $this->params['action'] == "logout"));
	}
	$output = '<ul class="nav">';
	foreach($nav as $key => $point){
		$classes = $key;
		if($key == "atelier")
			$classes .= " first";
		if($point['active'] == "active")
			$classes .= " active";
		
		$output .= '<li class="'.$classes.'">';
		$output .= '<a href="'.$point['Link'].'"><span>'.$point['Name'].'</span></a>';
		if(isset($submenu[$key])){
			$output .= '<ul class="sub">';
			foreach($submenu[$key] as $subKey => $subPoint){
				$classes = $subKey;
				if($subPoint['active'])
					$classes .= " active";
				
				$output .= '<li class="'.$classes.'"><a href="'.$subPoint['Link'].'">'.$subPoint['Name'].'</a></li>';
			}
			$output .= '</ul>';
		}
		
		$output .= '</li>';
	}
	$output .= '</ul>';
	echo $output;
?>