<div class="posts form">
	<?php echo $this->Form->create('title_price');?>
	<fieldset>
		<legend>Titel/Preisanfrage</legend>
		<?php
		echo $this->Form->input('id', array("label" => "Titel", 'max' => 255, 'type' => 'hidden'));
		echo $this->Form->input('title', array("label" => "Titel", 'max' => 255, 'type' => 'text'));
		echo $this->Form->input('price_note', array("label" => "Preisanfrage-Formular", 'max' => 255));
		?>
	</fieldset>
	<?php echo $this->Form->end('Abschicken', array('value' => 'Abschicken'));?>
</div>