<?php  
App::uses('AppHelper', 'View/Helper'); 
  
class TinymceHelper extends AppHelper { 
    
    // Take advantage of other helpers 
    public $helpers = array('Js', 'Html', 'Form'); 

    // Check if the tiny_mce.js file has been added or not 
    public $_script = false; 

    /** 
    * Adds the tiny_mce.js file and constructs the options 
    * 
    * @param string $fieldName Name of a field, like this "Modelname.fieldname" 
    * @param array $tinyoptions Array of TinyMCE attributes for this textarea 
    * @return string JavaScript code to initialise the TinyMCE area 
    */ 
    function _build($fieldName, $tinyoptions = array()){
        if(!$this->_script){ 
            // We don't want to add this every time, it's only needed once 
            $this->_script = true; 
            $this->Html->script('tinymce/tinymce.min', array('inline' => false)); 
        } 

        // Ties the options to the field 
        $tinyoptions['mode'] = 'exact'; 
		$tinyoptions['language'] = 'de'; 
        $tinyoptions['elements'] = $this->domId($fieldName); 

        // List the keys having a function 
        $value_arr = array(); 
        $replace_keys = array(); 
        foreach($tinyoptions as $key => &$value){ 
            // Checks if the value starts with 'function (' 
            if(strpos($value, 'function(') === 0){ 
                $value_arr[] = $value; 
                $value = '%' . $key . '%'; 
                $replace_keys[] = '"' . $value . '"'; 
            } 
        } 

        // Encode the array in json 
        $json = $this->Js->object($tinyoptions); 
        // Replace the functions 
        $json = str_replace($replace_keys, $value_arr, $json); 
        $this->Html->scriptStart(array('inline' => false)); 
        echo 'tinyMCE.init(' . $json . ');'; 
		
        $this->Html->scriptEnd(); 
    } 
  
    /** 
    * Creates a TinyMCE textarea. 
    * 
    * @param string $fieldName Name of a field, like this "Modelname.fieldname" 
    * @param array $options Array of HTML attributes. 
    * @param array $tinyoptions Array of TinyMCE attributes for this textarea 
    * @param string $preset 
    * @return string An HTML textarea element with TinyMCE 
    */ 
    function textarea($fieldName, $options = array(), $tinyoptions = array(), $preset = null){ 
        // If a preset is defined 
        if(!empty($preset)){ 
            $preset_options = $this->preset($preset); 

            // If $preset_options && $tinyoptions are an array 
            if(is_array($preset_options) && is_array($tinyoptions)){ 
                $tinyoptions = array_merge($preset_options, $tinyoptions); 
            }else{ 
                $tinyoptions = $preset_options; 
            } 
        } 
        return $this->Form->textarea($fieldName, $options) . $this->_build($fieldName, $tinyoptions); 
    } 
  
    /** 
    * Creates a TinyMCE textarea. 
    * 
    * @param string $fieldName Name of a field, like this "Modelname.fieldname" 
    * @param array $options Array of HTML attributes. 
    * @param array $tinyoptions Array of TinyMCE attributes for this textarea 
    * @return string An HTML textarea element with TinyMCE 
    */ 
    function input($fieldName, $options = array(), $tinyoptions = array(), $preset = null){ 
        // If a preset is defined 
        if(!empty($preset)){ 
            $preset_options = $this->preset($preset); 

            // If $preset_options && $tinyoptions are an array 
            if(is_array($preset_options) && is_array($tinyoptions)){ 
                $tinyoptions = array_merge($preset_options, $tinyoptions); 
            }else{ 
                $tinyoptions = $preset_options; 
            } 
        } 
        $options['type'] = 'textarea'; 
        return $this->Form->input($fieldName, $options) . $this->_build($fieldName, $tinyoptions); 
    } 
    
    /** 
    * Creates a preset for TinyOptions 
    * 
    * @param string $name 
    * @return array 
    */ 
    private function preset($name){ 
        // Simple 
        if($name == 'readonly'){ 
            return array( 
            	'toolbar' => false,
            	'menubar' => false,
            	'statusbar' => false,
                'readonly' => true,
				'width'=>'99%',
            ); 
        } 

        // BBCode 
        if($name == 'change'){ 
            return array( 
            	'width' => '99%',
            	'height' => '300',
            	'skin' => 'lightgray',
   	            'mode' => 'textareas',
   	            'menubar' => 'false' ,
   	            'plugins' => 'image',
   	            'toolbar' => ' underline bold italic | bullist numlist outdent indent | image ',
   	            'statusbar' => true,
   	            'relative_urls'=> 'false',
        	   ); 
        } 
        return null; 
    } 
}