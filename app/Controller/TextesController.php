<?php
/**
 * Static content controller.
 *
 * This file will render views from views/pages/
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('AppController', 'Controller');

/**
 * Static content controller
 *
 * Override this controller by placing a copy in controllers directory of an application
 *
 * @package       app.Controller
 * @link http://book.cakephp.org/2.0/en/controllers/pages-controller.html
 */
class TextesController extends AppController {

/**
 * This controller does not use a model
 *
 * @var array
 */
	public $uses = array("Texte");

/**
 * Displays a view
 *
 * @param mixed What page to display
 * @return void
 * @throws NotFoundException When the view file could not be found
 *	or MissingViewException in debug mode.
 */
	public function index() {
		$entrys = $this->Texte->find("all", array("conditions" => array("Texte.is_deleted" => 0), 'order' => array('modified' => 'asc')));
		
		$this->set('entrys', $entrys);
		$this->set('cat', "Texte");
		//print_r($this->Collection->find('all')); exit;
	}
	
	
	public function edit($id){
		$cat = "Texte";
		
		$this->set('cat', $cat);
		$entry = $this->Texte->find("first", array("conditions" => array("$cat.id" => $id, "$cat.is_deleted" => 0)));
		$uploaddir = WWW_ROOT.'uploads/'.$cat.'/';
		$this->request->data[$cat]['id'] = $id;
		$this->request->data[$cat]['user_id'] = $this->Auth->user('id');
		$this->request->data[$cat]['modified'] = date('Y-m-d H:i:s');
		
		
		
		if(isset($this->request->data[$cat]['pic_note_link']) && $this->request->data[$cat]['pic_note_link'] != "" && !filter_var($this->request->data[$cat]['pic_note_link'], FILTER_VALIDATE_URL)){
			$this->Session->setFlash('Es wurde keine gültige URL eingegeben, bitte überprüfen.');
			return false;
		}
		
		if((isset($this->request->data[$cat]['title']) && $this->request->data[$cat]['title'] != "") || (isset($this->request->data[$cat]['text']) && $this->request->data[$cat]['text'] != "")){
			
			
			for($i = 1; $i<5;$i++){
				if($this->request->data[$cat]['upload_'.$i]['type'] == 'image/jpeg' || $this->request->data[$cat]['upload_'.$i]['type'] == 'image/png' || !$this->request->data[$cat]['upload_'.$i]['type'] != ""){
					if($this->request->data[$cat]['upload_'.$i]['type'] == 'image/jpeg')
						$type = ".jpg";
					else 
						$type = ".png";
					
					$uploadfile = $uploaddir . basename($_FILES['data']['tmp_name'][$cat]['upload_'.$i]);
					if (!empty($this->request->data) && is_uploaded_file($this->request->data[$cat]['upload_'.$i]['tmp_name'])) {
						$fileName = str_replace("/", "_", $this->request->data[$cat]['upload_'.$i]['tmp_name']);
					    move_uploaded_file($this->request->data[$cat]['upload_'.$i]['tmp_name'], $uploaddir.$fileName.$type);
						$this->request->data[$cat]['pic_link_'.$i] = "/uploads/".$cat."/".$fileName.$type;
					}
				}
				else {
					$this->Session->setFlash('Bitte nur Bilder im jpg/jpeg oder png Format.');
					return false;
				}
			}


			$this->Texte->save($this->request->data);
			$this->redirect("/textes");
		}
		else{
			if($entry[$cat]['title'] != "")
				$this->request->data[$cat]['title'] = $entry[$cat]['title'];
			if($entry[$cat]['text'] != "")
				$this->request->data[$cat]['text'] = $entry[$cat]['text'];
			if($entry[$cat]['pic_note'] != "")
				$this->request->data[$cat]['pic_note'] = $entry[$cat]['pic_note'];
			if($entry[$cat]['pic_note_link'] != "")
				$this->request->data[$cat]['pic_note_link'] = $entry[$cat]['pic_note_link'];
			if($entry[$cat]['pic_link_1'] != "")
				$this->request->data[$cat]['pic_link_1'] = $entry[$cat]['pic_link_1'];
			if($entry[$cat]['pic_link_2'] != "")
				$this->request->data[$cat]['pic_link_2'] = $entry[$cat]['pic_link_2'];
			if($entry[$cat]['pic_link_3'] != "")
				$this->request->data[$cat]['pic_link_3'] = $entry[$cat]['pic_link_3'];
			if($entry[$cat]['pic_link_4'] != "")
				$this->request->data[$cat]['pic_link_4'] = $entry[$cat]['pic_link_4'];
			if($entry[$cat]['pic_link_5'] != "")
				$this->request->data[$cat]['pic_link_5'] = $entry[$cat]['pic_link_5'];
		}
	}
	
	
	public function create(){
		$cat = "Texte";
		
		$this->set('cat', $cat);
		$this->render("edit");
		
		$this->request->data[$cat]['user_id'] = $this->Auth->user('id');
		$this->request->data[$cat]['modified'] = date('Y-m-d H:i:s');
		
		
		$uploaddir = WWW_ROOT.'uploads/'.$cat.'/';
		
		
		if ($this->request->is('post')) {
			if((isset($this->request->data[$cat]['title']) && $this->request->data[$cat]['title'] != "") || (isset($this->request->data[$cat]['text']) && $this->request->data[$cat]['text'] != "")){
						
				for($i = 1; $i<6; $i++){
					if(isset($this->request->data[$cat]['upload_'.$i]['type']) && $this->request->data[$cat]['upload_'.$i]['type'] != ""){	
						if($this->request->data[$cat]['upload_'.$i]['type'] == 'image/jpeg' || $this->request->data[$cat]['upload_'.$i]['type'] == 'image/png'){
							if($this->request->data[$cat]['upload_'.$i]['type'] == 'image/jpeg')
								$type = ".jpg";
							else 
								$type = ".png";
							
							$uploadfile = $uploaddir . basename($_FILES['data']['tmp_name'][$cat]['upload_'.$i]);
							if (!empty($this->request->data) && is_uploaded_file($this->request->data[$cat]['upload_'.$i]['tmp_name'])) {
								$fileName = str_replace("/", "_", $this->request->data[$cat]['upload_'.$i]['tmp_name']);
							    move_uploaded_file($this->request->data[$cat]['upload_'.$i]['tmp_name'], $uploaddir.$fileName.$type);
								$this->request->data[$cat]['pic_link_'.$i] = "/uploads/".$cat."/".$fileName.$type;
							}
							else{
								$this->request->data[$cat]['upload_'.$i] = "";
							}
						}
						else {
							$this->Session->setFlash('Bitte nur Bilder im jpg/jpeg oder png Format.');
							return false;
						}
					}
				}



				
				if(isset($this->request->data[$cat]['pic_note_link']) && $this->request->data[$cat]['pic_note_link'] != "" && !filter_var($this->request->data[$cat]['pic_note_link'], FILTER_VALIDATE_URL)){
					$this->Session->setFlash('Es wurde keine gültige URL eingegeben, bitte überprüfen.');
					return false;
				}
				
				
				$this->Texte->save($this->request->data);
				$this->redirect("/textes");
			}
			else {
				$this->Session->setFlash('Bitte Titel oder Inhalt eingeben.');	
			}
		}
	}
}