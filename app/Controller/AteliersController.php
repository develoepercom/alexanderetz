<?php
/**
 * Static content controller.
 *
 * This file will render views from views/pages/
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('AppController', 'Controller');

/**
 * Static content controller
 *
 * Override this controller by placing a copy in controllers directory of an application
 *
 * @package       app.Controller
 * @link http://book.cakephp.org/2.0/en/controllers/pages-controller.html
 */
class AteliersController extends AppController {
	
/**
 * This controller does not use a model
 *
 * @var array
 */
	public $uses = array("Atelier");
	var $helpers = array('Html','Form');

/**
 * Displays a view
 *
 * @param mixed What page to display
 * @return void
 * @throws NotFoundException When the view file could not be found
 *	or MissingViewException in debug mode.
 */
	public function index() {
		$entrys = $this->Atelier->find("all", array("conditions" => array("Atelier.is_deleted" => 0), 'order' => array('modified' => 'desc')));
		
		$this->set('entrys', $entrys);
		$this->set('cat', "Atelier");
	}
	
	public function edit($id) {
		$cat = "Atelier";
		$entry = $this->Atelier->find("first", array("conditions" => array("$cat.id" => $id, "$cat.is_deleted" => 0)));
		$uploaddir = WWW_ROOT.'uploads/'.$cat.'/';
		$this->set('cat', $cat);
		
		$this->request->data[$cat]['id'] = $id;
		$this->request->data[$cat]['user_id'] = $this->Auth->user('id');
		$this->request->data[$cat]['modified'] = date('Y-m-d H:i:s');
		
		if(isset($this->request->data[$cat]['pic_note_link']) && $this->request->data[$cat]['pic_note_link'] != "" && !filter_var($this->request->data[$cat]['pic_note_link'], FILTER_VALIDATE_URL)){
			$this->Session->setFlash('Es wurde keine gültige URL eingegeben, bitte überprüfen.');
			return false;
		}
		
		if((isset($this->request->data[$cat]['title']) && $this->request->data[$cat]['title'] != "") || (isset($this->request->data[$cat]['text']) && $this->request->data[$cat]['text'] != "")){
			if($this->request->data[$cat]['upload']['type'] == 'image/jpeg' || $this->request->data[$cat]['upload']['type'] == 'image/png' || !$this->request->data[$cat]['upload']['type'] != ""){
				if($this->request->data[$cat]['upload']['type'] == 'image/jpeg')
					$type = ".jpg";
				else if($this->request->data[$cat]['upload']['type'] == 'image/png')
					$type = ".png";
					
				$uploadfile = $uploaddir . basename($_FILES['data']['tmp_name'][$cat]['upload']);
				if (!empty($this->request->data) && is_uploaded_file($this->request->data[$cat]['upload']['tmp_name'])) {
					$fileName = str_replace("/", "_", $this->request->data[$cat]['upload']['tmp_name']);
				    move_uploaded_file($this->request->data[$cat]['upload']['tmp_name'], $uploaddir.$fileName.$type);
					$this->request->data[$cat]['pic_link'] = "/uploads/".$cat."/".$fileName.$type;
				}
			}
			else {
				$this->Session->setFlash('Bitte nur Bilder im jpg/jpeg oder png Format.');
				return false;
			}
			
			if($this->request->data[$cat]['big_upload']['type'] == 'image/jpeg' || $this->request->data[$cat]['big_upload']['type'] == 'image/png' || !$this->request->data[$cat]['big_upload']['type'] != ""){
				if($this->request->data[$cat]['big_upload']['type'] == 'image/jpeg')
					$type = ".jpg";
				else 
					$type = ".png";
				
				$uploadfile = $uploaddir . basename($_FILES['data']['tmp_name'][$cat]['big_upload']);
				if (!empty($this->request->data) && is_uploaded_file($this->request->data[$cat]['big_upload']['tmp_name'])) {
					$fileName = str_replace("/", "_", $this->request->data[$cat]['big_upload']['tmp_name']);
				    move_uploaded_file($this->request->data[$cat]['big_upload']['tmp_name'], $uploaddir.$fileName.$type);
					$this->request->data[$cat]['pic_big_link'] = "/uploads/".$cat."/".$fileName.$type;
				}
			}
			else {
				$this->Session->setFlash('Bitte nur Bilder im jpg/jpeg oder png Format.');
				return false;
			}
			$this->Atelier->save($this->request->data);
			$this->redirect("/ateliers");
		}
		else{
			if($entry[$cat]['title'] != "")
				$this->request->data[$cat]['title'] = $entry[$cat]['title'];
			if($entry[$cat]['text'] != "")
				$this->request->data[$cat]['text'] = $entry[$cat]['text'];
			if($entry[$cat]['pic_note'] != "")
				$this->request->data[$cat]['pic_note'] = $entry[$cat]['pic_note'];
			if($entry[$cat]['pic_note_link'] != "")
				$this->request->data[$cat]['pic_note_link'] = $entry[$cat]['pic_note_link'];
			if($entry[$cat]['pic_big_link'] != "")
				$this->request->data[$cat]['pic_big_link'] = $entry[$cat]['pic_big_link'];
			if($entry[$cat]['pic_link'] != "")
				$this->request->data[$cat]['pic_link'] = $entry[$cat]['pic_link'];
			
		}
		
	}
	
	public function create() {
		$cat = "Atelier";
		
		$this->set('cat', $cat);
		$this->render("edit");
		
		$this->request->data[$cat]['user_id'] = $this->Auth->user('id');
		$this->request->data[$cat]['modified'] = date('Y-m-d H:i:s');
		

		$uploaddir = WWW_ROOT.'uploads/'.$cat.'/';
		

		if ($this->request->is('post')) {
			if((isset($this->request->data[$cat]['title']) && $this->request->data[$cat]['title'] != "") || (isset($this->request->data[$cat]['text']) && $this->request->data[$cat]['text'] != "")){
				if(isset($this->request->data[$cat]['upload']['type']) && $this->request->data[$cat]['upload']['type'] != ""){	
					if($this->request->data[$cat]['upload']['type'] == 'image/jpeg' || $this->request->data[$cat]['upload']['type'] == 'image/png'){
						if($this->request->data[$cat]['upload']['type'] == 'image/jpeg')
							$type = ".jpg";
						else 
							$type = ".png";
							
						$uploadfile = $uploaddir . basename($_FILES['data']['tmp_name'][$cat]['upload']);
						if (!empty($this->request->data) && is_uploaded_file($this->request->data[$cat]['upload']['tmp_name'])) {
							$fileName = str_replace("/", "_", $this->request->data[$cat]['upload']['tmp_name']);
						    move_uploaded_file($this->request->data[$cat]['upload']['tmp_name'], $uploaddir.$fileName.$type);
							$this->request->data[$cat]['pic_link'] = "/uploads/".$cat."/".$fileName.$type;
						}
						else{
							$this->request->data[$cat]['pic_link'] = "";	
						}
					}
					else {
						$this->Session->setFlash('Bitte nur Bilder im jpg/jpeg oder png Format.');
						return false;
					}
				}
				if(isset($this->request->data[$cat]['big_upload']['type']) && $this->request->data[$cat]['big_upload']['type'] != ""){	
					if($this->request->data[$cat]['big_upload']['type'] == 'image/jpeg' || $this->request->data[$cat]['big_upload']['type'] == 'image/png'){
						if($this->request->data[$cat]['big_upload']['type'] == 'image/jpeg')
							$type = ".jpg";
						else 
							$type = ".png";
						
						$uploadfile = $uploaddir . basename($_FILES['data']['tmp_name'][$cat]['big_upload']);
						if (!empty($this->request->data) && is_uploaded_file($this->request->data[$cat]['big_upload']['tmp_name'])) {
							$fileName = str_replace("/", "_", $this->request->data[$cat]['big_upload']['tmp_name']);
						    move_uploaded_file($this->request->data[$cat]['big_upload']['tmp_name'], $uploaddir.$fileName.$type);
							$this->request->data[$cat]['pic_big_link'] = "/uploads/".$cat."/".$fileName.$type;
						}
						else{
							$this->request->data[$cat]['pic_big_link'] = "";
						}
					}
					else {
						$this->Session->setFlash('Bitte nur Bilder im jpg/jpeg oder png Format.');
						return false;
					}
				}
				
				if(isset($this->request->data[$cat]['pic_note_link']) && $this->request->data[$cat]['pic_note_link'] != "" && !filter_var($this->request->data[$cat]['pic_note_link'], FILTER_VALIDATE_URL)){
					$this->Session->setFlash('Es wurde keine gültige URL eingegeben, bitte überprüfen.');
					return false;
				}
				
				$this->Atelier->save($this->request->data);
				$this->redirect("/ateliers");
			}
			else {
				$this->Session->setFlash('Bitte Titel oder Inhalt eingeben.');	
			}
		}
	}
}