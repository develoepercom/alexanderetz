<?php
/**
 * Static content controller.
 *
 * This file will render views from views/pages/
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('AppController', 'Controller');
App::uses('CakeEmail', 'Network/Email');


/**
 * Static content controller
 *
 * Override this controller by placing a copy in controllers directory of an application
 *
 * @package       app.Controller
 * @link http://book.cakephp.org/2.0/en/controllers/pages-controller.html
 */
class ContactsController extends AppController {

/**
 * This controller does not use a model
 *
 * @var array
 */
	public $uses = array();

/**
 * Displays a view
 *
 * @param mixed What page to display
 * @return void
 * @throws NotFoundException When the view file could not be found
 *	or MissingViewException in debug mode.
 */
	public function index() {
		
	}
	
	public function contact() {
		if ($this->request->is('post')){
			if((isset($this->request->data['Contact']['name']) && $this->request->data['Contact']['name'] != "") &&
				(isset($this->request->data['Contact']['email']) && $this->request->data['Contact']['email'] != "") &&
				(isset($this->request->data['Contact']['message']) && $this->request->data['Contact']['message'] != "")
			){
				$name = $this->request->data['Contact']['name'];
				$email = $this->request->data['Contact']['email'];
				$message = $this->request->data['Contact']['message'];
				$telephone = $this->request->data['Contact']['telephone'];
				
				$Email = new CakeEmail('smtp');
				$Email->viewVars(array('name' => $name, 'email' => $email, 'message' => $message, 'telephone' => $telephone));
				$Email->template('contact');
		    		$Email->emailFormat('html');
				$Email->from(array('info.alexander.etz@gmail.com' => 'Anfrage Webseite'));
			    	$Email->to('pujkrebs@aol.com');
				//$Email->cc('loeper@bucs.it');
			    $Email->subject($this->request->data['Contact']['name']);
			    if($Email->send()){
			    	$this->Session->setFlash('Erfolgreich verschickt.');	
			    }
			}
			else{
				$this->Session->setFlash('Bitte die Eingabefelder überprüfen');
			}
		}
		
	}
	
	public function impressum() {
		
	}
}